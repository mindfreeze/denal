use std::env;
use std::io::BufReader;
use std::io::Read;
use std::fs::File;

/// A NAL Unit is 0x00000001 where it is zero byte+ start_code_prefix_one_3bytes
/// ie. 0x00 + 0x000001[0x00 0x00 0x01] ie 4 bytes
const NAL_START: [u8; 4] = [0x00, 0x00, 0x00, 0x01];
/// Defined in Section B.3 of Annex B, When the three-byte sequence 0x000003 is
/// detected, the third byte (0x03) is an emulation_prevention_three_byte to be
///  discarded as specified in clause 7.4.1.
const ZERO_PREVENTION: [u8; 4] = [0x00, 0x00, 0x00, 0x03];
/// Custom Garbage detection
const G_START: [u8; 6] = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00];

fn main() {
    match env::args().nth(1) {
        Some(x) => process_file(x),
        None => print_usage()
    };
}

fn print_usage() {
    println!("No input file specified");
    println!("Usage: dvrdecode <input_file>")
}

fn process_file(file_name: String) {
    println!("Processing {}", file_name);

    let file = match File::open(file_name) {
        Ok(file) => file,
        Err(e) => panic!("Failed to open input file: {}", e)
    };

    let bytes = BufReader::new(&file).bytes();
    let mut nal_overlap: usize = 0;
    let mut zero_prevention_start: usize = 0;
    let mut garbage_unit: usize = 0;
    let mut pos: usize = 0;
    for byte in bytes {
        if nal_overlap == NAL_START.len() {
            println!("Found NAL at 0x{:x}!", pos);
            nal_overlap = 0;
        }

        if zero_prevention_start == ZERO_PREVENTION.len() {
            println!("Found emulation_prevention_three_byte at 0x{:x}!", pos);
            zero_prevention_start=0;
        }

        if garbage_unit == G_START.len() {
           // println!("Found Garbage at 0x{:x}!",pos);
            garbage_unit=0;
        }

        match byte {
            Ok(val) => {
                if val == NAL_START[nal_overlap] {
                    nal_overlap = nal_overlap + 1;
                } else {
                    nal_overlap = 0;
                }

                if val == ZERO_PREVENTION[zero_prevention_start]{
                    zero_prevention_start = zero_prevention_start+1;
                } else{
                    zero_prevention_start = 0;
                }

                if val == G_START[garbage_unit]{
                    garbage_unit = garbage_unit+1;
                } else{
                    garbage_unit = 0;
                }
            },
            Err(e) => {
                panic!("Failed to read from file: {}", e);
            }
        }

        pos += 1;
    }
}
